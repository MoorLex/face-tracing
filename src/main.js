import './styles/app.scss';

window.opencvIsReady = () => { new App() };
let canvas = document.getElementById('canvas');
let view = document.getElementById('camera--view');
let overlay = document.getElementById('camera--overlay');
let buffer = document.getElementById('camera--buffer');

let buffer_ctx = buffer.getContext('2d');
let ctx = overlay.getContext('2d');

class Camera {

  stream = undefined;

  constructor () {

    let constraints = { video: { facingMode: 'user' }, audio: false };

    return new Promise((resolve, reject) => {
      this.getDevices()
        .then((devices) => {

          constraints.deviceId = devices[0];

          this.getStream(constraints)
            .then((stream) => {
              this.stream = stream;
              resolve(stream);
            })
        })
    })
  }

  start () {
    return this.getCamera()
        .then((stream) => {
          this.stream = stream;
        })
        .catch((error) => {
          if(this.mode === 'front') {
            this.mode = 'back';
            return this.getCamera();
          }
        });
  }

  getDevices () {
    return navigator.mediaDevices.enumerateDevices()
      .then((devices) => {
        let arr = [];

        devices.map((item) => {
          if(item.kind === 'videoinput')
            arr.push(item.deviceId)
        })

        return arr;
      })
  }

  getStream (constraints) {
    return navigator.mediaDevices.getUserMedia(constraints)
      .then((stream) => {
        return stream;
      })
  }
}

function App () {
  let camera = new Camera();

  let height = view.offsetHeight;
  let width = view.offsetWidth;

  let detectFace = true;
  let faceClassifier = null;
  let srcMat = null;
  let grayMat = null;

  camera.then((stream) => {
    view.srcObject = stream;

    view.addEventListener( "loadedmetadata", (data) => {
      resize();
      start();
    }, false );
  })

  function resize() {
    height = view.offsetHeight;
    width = view.offsetWidth;
  }

  function start() {

    srcMat = new cv.Mat(height, width, cv.CV_8UC4);
    grayMat = new cv.Mat(height, width, cv.CV_8UC1);

    faceClassifier = new cv.CascadeClassifier();
    faceClassifier.load('haarcascade_frontalface_default.xml');

    render();
  }

  function render() {
    if (width && height) resizeCanvas();
    if (detectFace) faceTracing();
    requestAnimationFrame(render);
  }

  function resizeCanvas() {
    overlay.style.width = width+'px';
    overlay.style.height = height+'px';
    overlay.width = width;
    overlay.height = height;

    buffer.style.width = width+'px';
    buffer.style.height = height+'px';
    buffer.width = width;
    buffer.height = height;
  }

  function faceTracing() {

    buffer_ctx.drawImage(view, 0, 0, width, height);
    let imageData = buffer_ctx.getImageData(0, 0, width, height);

    srcMat.data.set(imageData.data);

    cv.cvtColor(srcMat, grayMat, cv.COLOR_RGBA2GRAY);

    let faces = [];
    let size;

    let faceVect = new cv.RectVector();
    let faceMat = new cv.Mat();

    cv.pyrDown(grayMat, faceMat);
    cv.pyrDown(faceMat, faceMat);
    size = faceMat.size();

    faceClassifier.detectMultiScale(faceMat, faceVect);

    for (let i = 0; i < faceVect.size(); i++) {
      let face = faceVect.get(i);
      faces.push(new cv.Rect(face.x, face.y, face.width, face.height));
    }
    faceMat.delete();
    faceVect.delete();

    drawResults(ctx, faces, 'red', size);
  }

  function drawResults(ctx, results, color, size) {
    ctx.clearRect(0, 0, width, height);

    for (let i = 0; i < results.length; ++i) {
      let rect = results[i];
      let xRatio = overlay.offsetWidth/size.width;
      let yRatio = overlay.offsetHeight/size.height;
      ctx.lineWidth = 3;
      ctx.strokeStyle = color;
      ctx.strokeRect(rect.x*xRatio, rect.y*yRatio, rect.width*xRatio, rect.height*yRatio);
    }
  }

  window.addEventListener('resize', resize);
}
